/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_DiegoAlejandroIvan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ivant_000
 */
public class MetodosBBDD 
{
    /**
     * metodo que recibe un tipo de plato (primero, segundo o otros), y los carga en una lista.
     * @param idTipo 
     * @return 
     */
    public static List<Producto> CargarPlatos(int idTipo)
    {
        List<Producto> platos=new ArrayList();
        Connection conexion = BaseDatos.getInstance().getConnection();
        try 
        {
            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia.executeQuery("SELECT * FROM comida where idtipo="+idTipo);
            Producto p1;
            while (rs.next()) {
                int idComida= rs.getInt("idcom");
                String nombre = rs.getString("nombre");  // rs.getString(1);
                double precio=Double.parseDouble(rs.getString("precio"));
                p1=new Producto(idComida, nombre, precio);
                platos.add(p1);
            }
        }catch(SQLException sqle)
        {
        } 
        return platos;
    }
    /**
     * Metodo que recibe unos parametros y los inserta en las campos de la tabla factura
     * @param nMesa
     * @param precio 
     */
    public static void insertarFactura(int nMesa, double precio)
    {
        Connection conexion = BaseDatos.getInstance().getConnection();
        try {
            Statement sentencia = conexion.createStatement();
            String sql = "INSERT INTO facturas(mesa,ptotal) VALUE ("
                    +nMesa+","+ precio +")";
            System.out.println(sql);
            sentencia.executeUpdate(sql);
        }
        catch(SQLException sqle)
        {
        }
    }
    /**
     * Metodo que recibe un producto, y la cantidad de ese producto, y accediendo a los atributos necesarios inserta una fila en la tabla nfactura
     * @param f
     * @param cantidad 
     */
    public static void insertarProductosFactura(Producto f)
    {
        Connection conexion = BaseDatos.getInstance().getConnection();
        try {
            Statement sentencia = conexion.createStatement();
            String sql = "INSERT INTO nfactura VALUE (" +
                    numeroFactura() + ","+f.getIdComida()+"," + f.getCantidad() + ","+f.getPrecio()*f.getCantidad()+")";
            System.out.println(sql);
            sentencia.executeUpdate(sql);
        }
        catch(SQLException sqle)
        {
        }
    }
    /**
     * Metodo que recibe un numero de factura y devulve la factura completa con todos los platos que tiene
     * @param nFactura
     * @return 
     */
    public static List<Factura> devolverFactura(int nFactura)
    {
        List<Factura> factura=new ArrayList();
        Connection conexion = BaseDatos.getInstance().getConnection();
        try 
        {
            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia.executeQuery("SELECT * FROM (nfactura inner join factura on factura.numfac=nfactura.numfac) inner join comida on comida.idcom=factura.idcom where idtipo=()");
            Factura p1;
            while (rs.next()) {
                int numFactura=rs.getInt("factura.numfac");
                int idComida= rs.getInt("comida.idcom");
                String nombre= rs.getString("nombre");
                int cantidad=rs.getInt("cantidad");
                double precio=rs.getDouble("nfactura.precio");
                p1=new Factura(numFactura,idComida,nombre,cantidad, precio);
                factura.add(p1);
            }
        }catch(SQLException sqle)
        {
        } 
        return factura;
    }
    /**
     * Metodo que nos devuelve un entero que corresponde al ultimo numero de factura insertado en la tabla factura
     * @return 
     */
    private static int numeroFactura()
    {
        int numfactura=0;
        Connection conexion = BaseDatos.getInstance().getConnection();
        try 
        {
            Statement sentencia = conexion.createStatement();
            ResultSet rs = sentencia.executeQuery("SELECT nfac FROM facturas order by nfac desc limit 1");
            rs.next();
            numfactura=rs.getInt(1);
        }catch(SQLException sqle)
        {
            System.out.println(sqle.getMessage());
        } 
        return numfactura;
    } 
}
