package proyecto_DiegoAlejandroIvan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ivan
 */
public class BaseDatos 
{
    private final String bd="comandas";
    private final String login="admin";
    private final String password="admin";
    private final String url="jdbc:mysql://10.0.13.93:3306/"+bd;
    private Connection conn;
    private static BaseDatos INSTANCE;

    /**
     * Patrón de diseño singleton
     */
    private BaseDatos() 
    {
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn=DriverManager.getConnection(url,login,password);
            if(conn!=null){
				System.out.println("Conexión a la base de datos "+url+".....CORRECTA");
            }
        }
        catch(SQLException | InstantiationException | IllegalAccessException ex){
            System.err.println("Problemas al conectar");
        }
        catch(ClassNotFoundException ex){
            System.err.println(ex.toString());
        }
    }
    
    public static BaseDatos getInstance()
    {
        if(INSTANCE == null)
            INSTANCE = new BaseDatos();
        return INSTANCE;
    }
    
    
    public Connection getConnection()
    {
        return conn;
    }
    
    

    
}