/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_DiegoAlejandroIvan;

import java.io.Serializable;

/**
 *Clase Mesa que posee 3 atributos pribados, un String nombre que indica el nombre de la mesa, un int sillas que indica el numero de comensales que entraran en la mesa, y un boolean ocupado que indica si la mesa esta ocupada.
 * posee metodos get y set para modificar los atributos
 * @author ivant_000
 */
public class Mesa implements Serializable
{
    private String nombre;
    private int sillas;
    private boolean ocupada;

    /**
     * Constructor de la clase que recibe dos parametros, el nombre de la mesa y el numero de sillas e inicia los atributos con los valores pasados, excepto el booleano que lo inicia en false(mesa libre).
     * @param nombre
     * @param sillas 
     * @param ocupada 
     */
    public Mesa(String nombre, int sillas, boolean ocupada) {
        this.nombre = nombre;
        this.sillas = sillas;
        this.ocupada = ocupada;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSillas() {
        return sillas;
    }

    public void setSillas(int sillas) {
        this.sillas = sillas;
    }

    public boolean isOcupada() {
        return ocupada;
    }

    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }
    
}
