/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_DiegoAlejandroIvan;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

/**
 *
 * @author DAM108
 */
public class Principal extends javax.swing.JFrame {
    private List<Producto> ListaProductosAñadidos;
    private DefaultListModel<String> ModelProductosAñadidos;
    private DefaultListModel<Producto> ModelPlatosPrincipales;
    private List<Producto> listaPlatosPrincipales;
    private DefaultListModel<Producto> ModelPlatosSegundos;
    private List<Producto> listaPlatosSegundos;
    private DefaultListModel<Producto> ModelPlatosOtros;
    private List<Producto> listaPlatosOtros;
    private boolean cambio=false;
    private Comedor c = new Comedor();

    public Principal() {
        initComponents();
        this.setResizable(false);
        this.setSize(550, 700);
        this.setTitle("Bar Miguel Herrero");
        this.setLocationRelativeTo(null);
        arrancarPrograma();
        adapta("Imagenes/ImagenEntrada.jpg");
        adaptaBotones("Imagenes/Comedor.jpg", Botoncomedor);
        adaptaBotones("Imagenes/Comandas.png", Botoncomandas);
        Principal.setVisible(true);
        PanelComedor.setVisible(false);
        textura.setVisible(false);
        PanelComanda.setVisible(false);
        textura3.setVisible(false);
        cambio=false;
        ListaProductosAñadidos = new ArrayList();
    }
    /**
     * metodo qu recibe un tipo de plato y las listas de la clase para inicializar en las listas los platos equivalentes al tipo de plato pasado por parametro
     * @param tipo
     * @param ListaGrafica
     * @param modelLista
     * @param listaPlatos 
     */
    public void crearLista(int tipo,JList<String> ListaGrafica, DefaultListModel modelLista, List<Producto> listaPlatos)
    {
        int contador=0;
        listaPlatos.addAll(MetodosBBDD.CargarPlatos(tipo));
        modelLista = new DefaultListModel();
        
        for (Iterator<Producto> iterator = listaPlatos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            modelLista.add(contador,next.getNombre());
            contador++;
        }
        ListaGrafica.setModel(modelLista);
        ListaGrafica.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    /**
     * Metodo que llama al metodo para crear las listas, pasandole los distintos tipos de platos y su correspondiente listas
     */
    private void rellenarListas()
    {
        listaPlatosPrincipales = new ArrayList();
        listaPlatosSegundos = new ArrayList();
        listaPlatosOtros = new ArrayList();
        crearLista(1, ListaPrimeros, ModelPlatosPrincipales, listaPlatosPrincipales);
        crearLista(2, ListaSegundos, ModelPlatosSegundos, listaPlatosSegundos);
        crearLista(3, ListaOtros, ModelPlatosOtros, listaPlatosOtros);
    }
    /**
     * Metodo que recibe un nombre y una lista, y busca el objeto con ese nombre en la lista pasada, para modificar su precio y cargarlo en la lista de productos añadidos
     * @param nombre
     * @param listaPlatos 
     */
    private void buscarProducto(String nombre, List<Producto> listaPlatos)
    {
        
        for (Iterator<Producto> iterator = listaPlatos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            if(next.getNombre().equalsIgnoreCase(nombre))
            {
                next.setCantidad((int)CantidadProducto.getValue());
                ListaProductosAñadidos.add(next);
            }
        }
        
    }
    /**
     * Metodo que elimina el elemento seleccionado en la lista de productos añadidos
     */
    private void eliminarPedido()
    {
        int index=ListaAñadidos.getSelectedIndex();
        ListaProductosAñadidos.remove(index);
        añadirListaPedido();
        sumarCantidad();
    }
    /**
     * Metodo que añade los productos de la lista d productos añadidos a la lista final de la factura
     */
    private void añadirListaPedido()
    {
        
        int contador=0;
        ModelProductosAñadidos = new DefaultListModel();
        
        for (Iterator<Producto> iterator = ListaProductosAñadidos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            ModelProductosAñadidos.add(contador,next.getCantidad()+"x "+next.getNombre()+" ("+next.getPrecio()+"€)");
            contador++;
        }
        ListaAñadidos.setModel(ModelProductosAñadidos);
        ListaAñadidos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    /**
     * Metodo que recorre la lista de productos final de factura y carga los elementos en una cadena string para cargarla en el panel de texto de la factura
     */
    private void añadirTextDialogo()
    {
        String cadena="";
        for (Iterator<Producto> iterator = ListaProductosAñadidos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            cadena+=next.getCantidad()+"x "+next.getNombre()+" ("+next.getPrecio()+"€) \n";
        }
        TextFacturaEnDialog.setText(cadena);
    }
    /**
     * Metodo que va sumando los precion de la lista de productos añadidos para obtener el precio final y cargarlo en una label a la vez que devolverlo
     * @return devulve un double con el precio total de los productos actualmente añadidos
     */
    private double sumarCantidad()
    {
        double cantidadTotal=0;
        for (Iterator<Producto> iterator = ListaProductosAñadidos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            cantidadTotal+=next.getCantidad()*next.getPrecio();
            
        }
        String cadena=cantidadTotal+"€";
        LabelPrecio.setText(cadena);
        return cantidadTotal;
    }
    /**
     * Metodo que añade a la lista de productos pedidos el elemento que este seleccionado en una de las listas de platos llamando al metodo buscar producto, tambien llama al metodo sumarCantidad para actualizar el precio total
     */
    private void añadirProducto()
    {
        if(!ListaPrimeros.isSelectionEmpty())
            buscarProducto(ListaPrimeros.getSelectedValue(),listaPlatosPrincipales);
        else if(!ListaSegundos.isSelectionEmpty())
            buscarProducto(ListaSegundos.getSelectedValue(), listaPlatosSegundos);
        else if(!ListaOtros.isSelectionEmpty())
            buscarProducto(ListaOtros.getSelectedValue(), listaPlatosOtros);
        else
            JOptionPane.showMessageDialog(null,"Seleccione un alimento a añadir","Error",JOptionPane.WARNING_MESSAGE);
        
        sumarCantidad();
        añadirListaPedido();
    }
    /**
     * Metodo que habre la ventana de la factura con los datos de la factra y la mesa correspondiente
     */
    private void botonFinalizar()
    {
        int[] array={1,2,3,4};
        DialogoFinalizarComanda.setVisible(true);
        DialogoFinalizarComanda.setSize(355, 550);
        DialogoFinalizarComanda.setLocationRelativeTo(null);
        int n=ComboNMesas.getSelectedIndex();
        LabelNMesa.setText(""+array[n]);
        añadirTextDialogo();
        LabelPrecioEnDialog.setText(sumarCantidad()+"€");
        
    }
    /**
     * Metodo que carga los datos de la factura en la base de datos y deja la lista de productos añadidos vacia de nuevo
     */
    private void confirmarFactura()
    {
        int[] array={1,2,3,4};
        int n=ComboNMesas.getSelectedIndex();
        MetodosBBDD.insertarFactura(array[n], sumarCantidad());
        for (Iterator<Producto> iterator = ListaProductosAñadidos.iterator(); iterator.hasNext();) {
            Producto next = iterator.next();
            
            MetodosBBDD.insertarProductosFactura(next);
        }
        ListaProductosAñadidos.removeAll(ListaProductosAñadidos);
        añadirListaPedido();
        sumarCantidad();
        DialogoFinalizarComanda.setVisible(false);
    }
    /**
     * Metodo que en el caso de que se halla hecho alguna modificacion guarda los cambios en el fichero
     */
    private void botonSalir()
    {
        if(cambio)
        {
            int num;
        num=JOptionPane.showConfirmDialog(null, "¿Desea guardar cambios?","Guardar", 1, 3);
            if(num==0)
            {
                c.guardarEnFichero();
                System.exit(0);
            }else if(num==1)
            {
                System.exit(0);
            }
        }else
            System.exit(0);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPopupMenuComedor = new javax.swing.JPopupMenu();
        MesasOcupar = new javax.swing.JMenuItem();
        MesasVaciar = new javax.swing.JMenuItem();
        DialogoFinalizarComanda = new javax.swing.JDialog();
        jLabel9 = new javax.swing.JLabel();
        LabelNMesa = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        TextFacturaEnDialog = new javax.swing.JTextArea();
        LabelPrecioEnDialog = new javax.swing.JLabel();
        DialogoConfirmar = new javax.swing.JButton();
        DialogoCancelar = new javax.swing.JButton();
        jPopupMenuEliminar = new javax.swing.JPopupMenu();
        jMenuItemEliminar = new javax.swing.JMenuItem();
        Cabecera = new javax.swing.JPanel();
        Botoncomandas = new javax.swing.JButton();
        Botoncomedor = new javax.swing.JButton();
        Principal = new javax.swing.JPanel();
        imagen = new javax.swing.JLabel();
        EntrarRestaurante = new javax.swing.JButton();
        PanelComedor = new javax.swing.JPanel();
        jCheckmesa1 = new javax.swing.JCheckBox();
        jCheckmesa2 = new javax.swing.JCheckBox();
        jCheckmesa3 = new javax.swing.JCheckBox();
        jCheckmesa4 = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        Mesa1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        EstadoMesa1 = new javax.swing.JLabel();
        Mesa2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        EstadoMesa2 = new javax.swing.JLabel();
        Mesa3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        EstadoMesa3 = new javax.swing.JLabel();
        Mesa4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        EstadoMesa4 = new javax.swing.JLabel();
        PanelComanda = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        LabelPrecio = new javax.swing.JLabel();
        ComboNMesas = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        ListaPrimeros = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        ListaSegundos = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListaOtros = new javax.swing.JList<>();
        CantidadProducto = new javax.swing.JSpinner();
        Añadir = new javax.swing.JButton();
        Finalizar = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        ListaAñadidos = new javax.swing.JList<>();
        textura = new javax.swing.JLabel();
        textura2 = new javax.swing.JLabel();
        textura3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        Guardar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        Salir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        Acerca = new javax.swing.JMenuItem();

        MesasOcupar.setText("Ocupar mesas");
        MesasOcupar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MesasOcuparActionPerformed(evt);
            }
        });
        jPopupMenuComedor.add(MesasOcupar);

        MesasVaciar.setText("Vaciar mesas");
        MesasVaciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MesasVaciarActionPerformed(evt);
            }
        });
        jPopupMenuComedor.add(MesasVaciar);

        DialogoFinalizarComanda.setTitle("Resumen de la comanda");

        jLabel9.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        jLabel9.setText("MESA:");

        LabelNMesa.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N

        TextFacturaEnDialog.setEditable(false);
        TextFacturaEnDialog.setColumns(20);
        TextFacturaEnDialog.setRows(5);
        jScrollPane4.setViewportView(TextFacturaEnDialog);

        LabelPrecioEnDialog.setFont(new java.awt.Font("Arial", 1, 36)); // NOI18N
        LabelPrecioEnDialog.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        DialogoConfirmar.setText("Confirmar");
        DialogoConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DialogoConfirmarActionPerformed(evt);
            }
        });

        DialogoCancelar.setText("Cancelar");
        DialogoCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DialogoCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout DialogoFinalizarComandaLayout = new javax.swing.GroupLayout(DialogoFinalizarComanda.getContentPane());
        DialogoFinalizarComanda.getContentPane().setLayout(DialogoFinalizarComandaLayout);
        DialogoFinalizarComandaLayout.setHorizontalGroup(
            DialogoFinalizarComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoFinalizarComandaLayout.createSequentialGroup()
                .addGroup(DialogoFinalizarComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, DialogoFinalizarComandaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(DialogoConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(DialogoCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogoFinalizarComandaLayout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LabelNMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogoFinalizarComandaLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(DialogoFinalizarComandaLayout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(LabelPrecioEnDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        DialogoFinalizarComandaLayout.setVerticalGroup(
            DialogoFinalizarComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DialogoFinalizarComandaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DialogoFinalizarComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(LabelNMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(LabelPrecioEnDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(DialogoFinalizarComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(DialogoConfirmar, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(DialogoCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPopupMenuEliminar.setComponentPopupMenu(jPopupMenuEliminar);

        jMenuItemEliminar.setText("Eliminar");
        jMenuItemEliminar.setToolTipText("");
        jMenuItemEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEliminarActionPerformed(evt);
            }
        });
        jPopupMenuEliminar.add(jMenuItemEliminar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        Cabecera.setBackground(new java.awt.Color(255, 255, 51));
        Cabecera.setPreferredSize(new java.awt.Dimension(550, 40));

        Botoncomandas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotoncomandasActionPerformed(evt);
            }
        });

        Botoncomedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotoncomedorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CabeceraLayout = new javax.swing.GroupLayout(Cabecera);
        Cabecera.setLayout(CabeceraLayout);
        CabeceraLayout.setHorizontalGroup(
            CabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CabeceraLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Botoncomedor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Botoncomandas)
                .addContainerGap(468, Short.MAX_VALUE))
        );
        CabeceraLayout.setVerticalGroup(
            CabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CabeceraLayout.createSequentialGroup()
                .addGroup(CabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Botoncomedor, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                    .addComponent(Botoncomandas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getContentPane().add(Cabecera);
        Cabecera.setBounds(0, 0, 550, 50);

        Principal.setBackground(new java.awt.Color(0, 204, 204));
        Principal.setOpaque(false);
        Principal.setPreferredSize(new java.awt.Dimension(550, 633));

        EntrarRestaurante.setText("Entrar al Restaurante");
        EntrarRestaurante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EntrarRestauranteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PrincipalLayout = new javax.swing.GroupLayout(Principal);
        Principal.setLayout(PrincipalLayout);
        PrincipalLayout.setHorizontalGroup(
            PrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(imagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PrincipalLayout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(EntrarRestaurante)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PrincipalLayout.setVerticalGroup(
            PrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PrincipalLayout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(imagen, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(155, 155, 155)
                .addComponent(EntrarRestaurante)
                .addGap(60, 60, 60))
        );

        getContentPane().add(Principal);
        Principal.setBounds(0, 50, 550, 630);

        PanelComedor.setBackground(new java.awt.Color(0, 0, 255));
        PanelComedor.setComponentPopupMenu(jPopupMenuComedor);
        PanelComedor.setOpaque(false);
        PanelComedor.setPreferredSize(new java.awt.Dimension(550, 633));

        jCheckmesa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckmesa1ActionPerformed(evt);
            }
        });

        jCheckmesa2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckmesa2ActionPerformed(evt);
            }
        });

        jCheckmesa3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckmesa3ActionPerformed(evt);
            }
        });

        jCheckmesa4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckmesa4ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel7.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel6.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel8.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel9.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel10.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel11.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel12.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel14.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        Mesa1.setPreferredSize(new java.awt.Dimension(100, 100));

        jLabel1.setText("Mesa1");

        EstadoMesa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout Mesa1Layout = new javax.swing.GroupLayout(Mesa1);
        Mesa1.setLayout(Mesa1Layout);
        Mesa1Layout.setHorizontalGroup(
            Mesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(EstadoMesa1, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(Mesa1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Mesa1Layout.setVerticalGroup(
            Mesa1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(EstadoMesa1, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                .addContainerGap())
        );

        Mesa2.setPreferredSize(new java.awt.Dimension(100, 100));

        jLabel2.setText("Mesa2");

        EstadoMesa2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout Mesa2Layout = new javax.swing.GroupLayout(Mesa2);
        Mesa2.setLayout(Mesa2Layout);
        Mesa2Layout.setHorizontalGroup(
            Mesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel2)
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(Mesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(Mesa2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(EstadoMesa2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        Mesa2Layout.setVerticalGroup(
            Mesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa2Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel2)
                .addContainerGap(60, Short.MAX_VALUE))
            .addGroup(Mesa2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Mesa2Layout.createSequentialGroup()
                    .addContainerGap(51, Short.MAX_VALUE)
                    .addComponent(EstadoMesa2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        Mesa3.setPreferredSize(new java.awt.Dimension(100, 100));

        jLabel3.setText("Mesa3");

        EstadoMesa3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout Mesa3Layout = new javax.swing.GroupLayout(Mesa3);
        Mesa3.setLayout(Mesa3Layout);
        Mesa3Layout.setHorizontalGroup(
            Mesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa3Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel3)
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Mesa3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(EstadoMesa3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        Mesa3Layout.setVerticalGroup(
            Mesa3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa3Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(EstadoMesa3, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addGap(22, 22, 22))
        );

        jLabel4.setText("Mesa4");

        javax.swing.GroupLayout Mesa4Layout = new javax.swing.GroupLayout(Mesa4);
        Mesa4.setLayout(Mesa4Layout);
        Mesa4Layout.setHorizontalGroup(
            Mesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa4Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel4)
                .addContainerGap(46, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Mesa4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(EstadoMesa4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        Mesa4Layout.setVerticalGroup(
            Mesa4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Mesa4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(EstadoMesa4, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout PanelComedorLayout = new javax.swing.GroupLayout(PanelComedor);
        PanelComedor.setLayout(PanelComedorLayout);
        PanelComedorLayout.setHorizontalGroup(
            PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelComedorLayout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jCheckmesa1)
                            .addComponent(jCheckmesa3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Mesa3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(PanelComedorLayout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(158, 158, 158)
                                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(PanelComedorLayout.createSequentialGroup()
                                        .addGap(27, 27, 27)
                                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(PanelComedorLayout.createSequentialGroup()
                                        .addComponent(jCheckmesa4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(Mesa4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addComponent(Mesa1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(158, 158, 158)
                                .addComponent(jCheckmesa2)
                                .addGap(6, 6, 6)
                                .addComponent(Mesa2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(67, Short.MAX_VALUE))
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(117, 117, 117))
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(118, 118, 118))))
        );
        PanelComedorLayout.setVerticalGroup(
            PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelComedorLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(jCheckmesa1))
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Mesa2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Mesa1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(jCheckmesa2)))
                .addGap(6, 6, 6)
                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(73, 73, 73)
                .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Mesa4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(jCheckmesa4)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelComedorLayout.createSequentialGroup()
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Mesa3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelComedorLayout.createSequentialGroup()
                                .addGap(44, 44, 44)
                                .addComponent(jCheckmesa3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelComedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(137, Short.MAX_VALUE))
        );

        getContentPane().add(PanelComedor);
        PanelComedor.setBounds(0, 0, 550, 633);

        PanelComanda.setOpaque(false);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("MESA:");

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("PRIMEROS:");

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setText("SEGUNDOS:");

        jLabel8.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel8.setText("OTROS:");

        LabelPrecio.setFont(new java.awt.Font("Arial", 1, 48)); // NOI18N

        ComboNMesas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        ComboNMesas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4" }));

        ListaPrimeros.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        ListaPrimeros.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ListaPrimerosValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(ListaPrimeros);

        ListaSegundos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        ListaSegundos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ListaSegundosValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(ListaSegundos);

        ListaOtros.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        ListaOtros.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ListaOtrosValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(ListaOtros);

        CantidadProducto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        CantidadProducto.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        Añadir.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        Añadir.setText("Añadir");
        Añadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñadirActionPerformed(evt);
            }
        });

        Finalizar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        Finalizar.setText("Finalizar");
        Finalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FinalizarActionPerformed(evt);
            }
        });

        ListaAñadidos.setComponentPopupMenu(jPopupMenuEliminar);
        jScrollPane6.setViewportView(ListaAñadidos);

        javax.swing.GroupLayout PanelComandaLayout = new javax.swing.GroupLayout(PanelComanda);
        PanelComanda.setLayout(PanelComandaLayout);
        PanelComandaLayout.setHorizontalGroup(
            PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelComandaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ComboNMesas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelComandaLayout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jLabel6)
                        .addGap(85, 85, 85)
                        .addComponent(jLabel7)
                        .addGap(91, 91, 91)
                        .addComponent(jLabel8))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelComandaLayout.createSequentialGroup()
                        .addGap(0, 7, Short.MAX_VALUE)
                        .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(PanelComandaLayout.createSequentialGroup()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Finalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(LabelPrecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(PanelComandaLayout.createSequentialGroup()
                                .addComponent(CantidadProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Añadir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(PanelComandaLayout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(27, 27, 27))
        );
        PanelComandaLayout.setVerticalGroup(
            PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelComandaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(ComboNMesas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane3))
                .addGap(18, 18, 18)
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CantidadProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Añadir, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(PanelComandaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PanelComandaLayout.createSequentialGroup()
                        .addComponent(LabelPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)
                        .addComponent(Finalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6))
                .addContainerGap(176, Short.MAX_VALUE))
        );

        getContentPane().add(PanelComanda);
        PanelComanda.setBounds(0, 50, 550, 630);

        textura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyecto_DiegoAlejandroIvan/textura.jpg"))); // NOI18N
        textura.setText("jLabel7");
        textura.setToolTipText("");
        getContentPane().add(textura);
        textura.setBounds(0, 0, 550, 680);

        textura2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyecto_DiegoAlejandroIvan/textura2.jpg"))); // NOI18N
        textura2.setText("jLabel7");
        getContentPane().add(textura2);
        textura2.setBounds(0, 0, 550, 680);

        textura3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyecto_DiegoAlejandroIvan/textura3.jpg"))); // NOI18N
        textura3.setText("jLabel7");
        getContentPane().add(textura3);
        textura3.setBounds(0, 0, 550, 680);

        jMenu1.setText("Archivo");

        Guardar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        Guardar.setText("Guardar");
        Guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardarActionPerformed(evt);
            }
        });
        jMenu1.add(Guardar);
        jMenu1.add(jSeparator1);

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });
        jMenu1.add(Salir);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Gestión");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Comedor");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Comandas");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");

        Acerca.setText("Acerca de...");
        Acerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AcercaActionPerformed(evt);
            }
        });
        jMenu3.add(Acerca);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AcercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AcercaActionPerformed
        JOptionPane.showMessageDialog(this,"Creado por: \nIvan Trueba Alonso\nDiego Fernández Díaz\nAlejandro Fernández Gutiérrez","Ayuda",JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_AcercaActionPerformed

    private void GuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardarActionPerformed
        cambio=false;
        c.guardarEnFichero();        
    }//GEN-LAST:event_GuardarActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        botonSalir();
    }//GEN-LAST:event_SalirActionPerformed

    private void EntrarRestauranteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EntrarRestauranteActionPerformed
        abrirComedor();
    }//GEN-LAST:event_EntrarRestauranteActionPerformed

    private void BotoncomedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotoncomedorActionPerformed
        abrirComedor();
    }//GEN-LAST:event_BotoncomedorActionPerformed

    private void jCheckmesa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckmesa1ActionPerformed
        cambio=true;
        seleccionarMesa(jCheckmesa1, Mesa1, EstadoMesa1,"mesa1");
        
    }//GEN-LAST:event_jCheckmesa1ActionPerformed

    private void jCheckmesa2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckmesa2ActionPerformed
        cambio=true;
        seleccionarMesa(jCheckmesa2, Mesa2, EstadoMesa2,"mesa2");
        
    }//GEN-LAST:event_jCheckmesa2ActionPerformed

    private void jCheckmesa3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckmesa3ActionPerformed
        cambio=true;
        seleccionarMesa(jCheckmesa3, Mesa3, EstadoMesa3,"mesa3");
        
    }//GEN-LAST:event_jCheckmesa3ActionPerformed

    private void jCheckmesa4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckmesa4ActionPerformed
        cambio=true;
        seleccionarMesa(jCheckmesa4, Mesa4, EstadoMesa4,"mesa4");
        
    }//GEN-LAST:event_jCheckmesa4ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        abrirComedor();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void MesasVaciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MesasVaciarActionPerformed
        cambio=true;
        vaciarMesas();
    }//GEN-LAST:event_MesasVaciarActionPerformed

    private void MesasOcuparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MesasOcuparActionPerformed
        cambio=true;
        ocuparMesas();
    }//GEN-LAST:event_MesasOcuparActionPerformed

    private void AñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñadirActionPerformed
        añadirProducto();
    }//GEN-LAST:event_AñadirActionPerformed

    private void BotoncomandasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotoncomandasActionPerformed
        abrirComanda();
    }//GEN-LAST:event_BotoncomandasActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        abrirComanda();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void ListaPrimerosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ListaPrimerosValueChanged
        ListaSegundos.clearSelection();
        ListaOtros.clearSelection();
    }//GEN-LAST:event_ListaPrimerosValueChanged

    private void ListaSegundosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ListaSegundosValueChanged
        ListaPrimeros.clearSelection();
        ListaOtros.clearSelection();
    }//GEN-LAST:event_ListaSegundosValueChanged

    private void FinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FinalizarActionPerformed
        botonFinalizar();
    }//GEN-LAST:event_FinalizarActionPerformed

    private void ListaOtrosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ListaOtrosValueChanged
        ListaPrimeros.clearSelection();
        ListaSegundos.clearSelection();
    }//GEN-LAST:event_ListaOtrosValueChanged

    private void jMenuItemEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarActionPerformed
        eliminarPedido();
    }//GEN-LAST:event_jMenuItemEliminarActionPerformed

    private void DialogoConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DialogoConfirmarActionPerformed
        confirmarFactura();
    }//GEN-LAST:event_DialogoConfirmarActionPerformed

    private void DialogoCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DialogoCancelarActionPerformed
        DialogoFinalizarComanda.setVisible(false);
    }//GEN-LAST:event_DialogoCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }
    /**
     * Metodo que arranca el programa llamado a otro metodo que comprueba si hay un fichero guardado, de ser asi lo cargara y despues comprueva el estado de las mesas.
     */
    private void arrancarPrograma() {
        String[] mesas = {"mesa1", "mesa2", "mesa3", "mesa4"};
        c.arrancarPrograma();
        for (Iterator iterator = c.getMesas().iterator(); iterator.hasNext();) {
            Mesa next = (Mesa) iterator.next();
            for (int i = 0; i < mesas.length; i++) {
                if (next.getNombre().equalsIgnoreCase(mesas[i])) {

                    switch (i+1) {
                        case 1:
                            if (next.isOcupada()) {
                                ocuparMesa(jCheckmesa1, Mesa1, EstadoMesa1,"mesa1");
                            } else {
                                vaciarMesa(jCheckmesa1, Mesa1, EstadoMesa1,"mesa1");
                            }

                            break;
                        case 2:
                            if (next.isOcupada()) {
                                ocuparMesa(jCheckmesa2, Mesa2, EstadoMesa2,"mesa2");
                            } else {
                                vaciarMesa(jCheckmesa2, Mesa2, EstadoMesa2,"mesa2");
                            }

                            break;
                        case 3:
                            if (next.isOcupada()) {
                                ocuparMesa(jCheckmesa3, Mesa3, EstadoMesa3,"mesa3");
                            } else {
                                vaciarMesa(jCheckmesa3, Mesa3, EstadoMesa3,"mesa3");
                            }

                            break;
                        case 4:
                            if (next.isOcupada()) {
                                ocuparMesa(jCheckmesa4, Mesa4, EstadoMesa4,"mesa4");
                            } else {
                                vaciarMesa(jCheckmesa4, Mesa4, EstadoMesa4,"mesa4");
                            }
                            break;
                    }
                }
            }
        }

    }
    /**
     * Metodo que sale de la ventana de inicio y habre el panel del comedor
     */
    private void abrirComedor() {
        Principal.setVisible(false);
        textura2.setVisible(false);
        PanelComanda.setVisible(false);
        textura3.setVisible(false);
        PanelComedor.setVisible(true);
        textura.setVisible(true);
    }
    /**
     * Metodo que habre los paneles correspondientes a las comandas y cierra los demas
     */
    private void abrirComanda() {
        DefaultComboBoxModel<String> combo=new DefaultComboBoxModel();
        List<Integer> ocupados = new ArrayList();
        ocupados.addAll(c.devolverMesasOcupadas());
        for (Iterator<Integer> iterator = ocupados.iterator(); iterator.hasNext();) {
            Integer next = iterator.next();
            combo.addElement(""+next);
        }
        ComboNMesas.setModel(combo);
        Principal.setVisible(false);
        textura2.setVisible(false);
        PanelComedor.setVisible(false);
        textura.setVisible(false);
        PanelComanda.setVisible(true);
        textura3.setVisible(true);
        rellenarListas();
    }
    /**
     * Metodo que ocupa todas las mesas
     */
    private void ocuparMesas() {
        
        ocuparMesa(jCheckmesa1, Mesa1, EstadoMesa1,"mesa1");
        ocuparMesa(jCheckmesa2, Mesa2, EstadoMesa2,"mesa2");
        ocuparMesa(jCheckmesa3, Mesa3, EstadoMesa3,"mesa3");
        ocuparMesa(jCheckmesa4, Mesa4, EstadoMesa4,"mesa4");
    }
    /**
     * Metodo que vacia todas las mesas
     */
    private void vaciarMesas() {
        
        vaciarMesa(jCheckmesa1, Mesa1, EstadoMesa1,"mesa1");
        vaciarMesa(jCheckmesa2, Mesa2, EstadoMesa2,"mesa2");
        vaciarMesa(jCheckmesa3, Mesa3, EstadoMesa3,"mesa3");
        vaciarMesa(jCheckmesa4, Mesa4, EstadoMesa4,"mesa4");
        
    }
    /**
     * Metodo que vacia una mesa
     * @param check
     * @param mesa
     * @param estado
     * @param nombre 
     */
    private void vaciarMesa(JCheckBox check, JPanel mesa, JLabel estado,String nombre) {
       
        mesa.setBackground(Color.GREEN);
        check.setSelected(false);
        estado.setText("LIBRE");
        c.desocupar(nombre);
        
    }
    /**
     * Metodo que recoje el estado de un checkbox y cambia el estado de la mesa a ocupado cambiando tambien su color
     * @param check
     * @param mesa
     * @param estado
     * @param nombre 
     */
    private void ocuparMesa(JCheckBox check, JPanel mesa, JLabel estado,String nombre)
    {
        
        mesa.setBackground(Color.RED);
        check.setSelected(true);
        estado.setText("OCUPADO");
        c.ocupar(nombre);
    }
    /**
     * Metodo que recoje el estado de un checkbox y cambia el estado de la mesa a libre cambiando tambien su color
     * @param check
     * @param mesa
     * @param estado
     * @param nombre 
     */
    private void seleccionarMesa(JCheckBox check, JPanel mesa, JLabel estado,String nombre) {

        if (check.isSelected()) {
            mesa.setBackground(Color.RED);
            estado.setText("OCUPADO");
            c.ocupar(nombre);

        } else {
            mesa.setBackground(Color.green);
            estado.setText("LIBRE");
            c.desocupar(nombre);
        }
    }
    /**
     * Metodo que recive una ruta de imagen y la situa adaptada en una etiqueta en la ventana de vienvenida.
     * @param ruta 
     */
    private void adapta(String ruta) {
        ImageIcon img = new ImageIcon(ruta);
        Icon icon = new ImageIcon(img.getImage().getScaledInstance(imagen.getWidth(),
                imagen.getHeight(), img.getIconWidth()));
        imagen.setIcon((icon));

    }
    /**
     * Metodo que recive una ruta de imagen y un boton, y adapta la imagen en el boton.
     * @param ruta
     * @param boton 
     */
    private void adaptaBotones(String ruta, JButton boton) {
        ImageIcon img = new ImageIcon(ruta);
        Icon icon = new ImageIcon(img.getImage().getScaledInstance(boton.getWidth(),
                boton.getHeight(), img.getIconWidth()));
        boton.setIcon((icon));

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Acerca;
    private javax.swing.JButton Añadir;
    private javax.swing.JButton Botoncomandas;
    private javax.swing.JButton Botoncomedor;
    private javax.swing.JPanel Cabecera;
    private javax.swing.JSpinner CantidadProducto;
    private javax.swing.JComboBox<String> ComboNMesas;
    private javax.swing.JButton DialogoCancelar;
    private javax.swing.JButton DialogoConfirmar;
    private javax.swing.JDialog DialogoFinalizarComanda;
    private javax.swing.JButton EntrarRestaurante;
    private javax.swing.JLabel EstadoMesa1;
    private javax.swing.JLabel EstadoMesa2;
    private javax.swing.JLabel EstadoMesa3;
    private javax.swing.JLabel EstadoMesa4;
    private javax.swing.JButton Finalizar;
    private javax.swing.JMenuItem Guardar;
    private javax.swing.JLabel LabelNMesa;
    private javax.swing.JLabel LabelPrecio;
    private javax.swing.JLabel LabelPrecioEnDialog;
    private javax.swing.JList<String> ListaAñadidos;
    private javax.swing.JList<String> ListaOtros;
    private javax.swing.JList<String> ListaPrimeros;
    private javax.swing.JList<String> ListaSegundos;
    private javax.swing.JPanel Mesa1;
    private javax.swing.JPanel Mesa2;
    private javax.swing.JPanel Mesa3;
    private javax.swing.JPanel Mesa4;
    private javax.swing.JMenuItem MesasOcupar;
    private javax.swing.JMenuItem MesasVaciar;
    private javax.swing.JPanel PanelComanda;
    private javax.swing.JPanel PanelComedor;
    private javax.swing.JPanel Principal;
    private javax.swing.JMenuItem Salir;
    private javax.swing.JTextArea TextFacturaEnDialog;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel imagen;
    private javax.swing.JCheckBox jCheckmesa1;
    private javax.swing.JCheckBox jCheckmesa2;
    private javax.swing.JCheckBox jCheckmesa3;
    private javax.swing.JCheckBox jCheckmesa4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItemEliminar;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenuComedor;
    private javax.swing.JPopupMenu jPopupMenuEliminar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel textura;
    private javax.swing.JLabel textura2;
    private javax.swing.JLabel textura3;
    // End of variables declaration//GEN-END:variables
}
