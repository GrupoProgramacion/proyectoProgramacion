/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_DiegoAlejandroIvan;

/**
 *
 * @author ivant_000
 */
public class Producto 
{
    private int idComida;
    private String nombre;
    private double precio;
    private int cantidad;


    public Producto(int idComida, String nombre, double precio) 
    {
        this.idComida=idComida;
        this.nombre = nombre;
        this.precio = precio;

    }

    public Producto(int idComida, String nombre, double precio, int cantidad) 
    {
        this.idComida = idComida;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad=cantidad;

    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdComida() {
        return idComida;
    }

    public void setIdComida(int idComida) {
        this.idComida = idComida;
    }

    
    public String getNombre() 
    {
        return nombre;
    }

    public void setNombre(String nombre) 
    {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) 
    {
        this.precio = precio;
    }
}
