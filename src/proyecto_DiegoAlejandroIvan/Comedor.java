/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_DiegoAlejandroIvan;

import java.awt.Color;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *Clase comedor que implementa la clase serializable para poder guardar los atributos serializados.
 * Un atributo pribado con una coleccion de mesas
 * @author ivant_000
 */
public class Comedor implements Serializable
{
   private ArrayList<Mesa> mesas;
    
    /**
     * Constructor de la clase, no recibe ningun parametro, he inicializa el ArrayList.
     */
    public Comedor() 
    {
        mesas=new ArrayList();
    }
    
    /**
     * Metodo que recibe una mesa, y debuelbe un booleano, que indica si se ha podido añadir esa mesa a la colección.
     * @param m
     * @return boolen
     */
    public boolean añadirmesa(Mesa m)
    {
        return this.mesas.add(m);
    }
    
    /**
     * Metodo que recorre la coleccion y la va guardando en un fichero serializado, ademas este metodo comprueba si el fichero ya tiene cabeceras.
     */
    public void guardarEnFichero()
    {
        File fichero=new File("EstadoMesas.dat");
        ObjectOutputStream oos=null;
        
        try 
        {
            
                oos= new ObjectOutputStream(new FileOutputStream(fichero, false));
            for(Mesa m:mesas)
                oos.writeObject(m);
        } catch (FileNotFoundException ex) {
            System.out.println("Fichero no encontrado");
        } catch (IOException ex) {
            System.out.println("Excepción de E/S "+ex.getMessage());
        }
        finally
        {
            if(oos!=null) 
            {     
                try{
                    oos.close();
                } catch (IOException ex) {
                        System.out.println("Error al cerrar el fichero");
                }
            }
        }
    }

    public ArrayList<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(ArrayList<Mesa> mesas) {
        this.mesas = mesas;
    }
    public void eliminarTodo()
    {
        mesas.removeAll(mesas);
    }
    public void añadirTodasMesas(ArrayList<Mesa> c)
    {
        mesas.addAll(c);
    }
    /**
     * Metodo que lee un fichero serializado y carga las mesas guardadas en la coleccion en la coleccion
     */
    public void cargarFichero()
    {
        File fichero=new File("EstadoMesas.dat");
        ObjectInputStream ois=null;
        try 
        {
            ois = new ObjectInputStream(new FileInputStream(fichero));
            Mesa m;
            while(true)
            {
                m=(Mesa)ois.readObject();
                añadirmesa(m);
            } 
        }catch(EOFException eofe){
            System.out.println("Fin de fichero");
        }catch (ClassNotFoundException ex1) {
            System.out.println("Error el tipo de objeto no es compatible");
        }catch (FileNotFoundException ex2) {
            System.out.println("Fichero no encontrado");
        }catch (IOException ex) {
            System.out.println("Excepción de E/S "+ex.getMessage());
        }
        finally
        {
            if(ois!=null)
                try {
                    ois.close();
                } catch (IOException ex) {
                    System.out.println("Error al cerrar");
                }
                
        }
    }
    /**
     * Metodo que devuelve una lista con las mesas ocupadas
     * @return List<Integer>
     */
    public List<Integer> devolverMesasOcupadas()
    {
        List ocupados= new ArrayList();
        for (Iterator<Mesa> iterator = mesas.iterator(); iterator.hasNext();) {
            Mesa next = iterator.next();
            if(next.isOcupada())
            {
                ocupados.add(Integer.parseInt(next.getNombre().substring(next.getNombre().length()-1)));
            }
        }
        return ocupados;
    }
    /**
     * Metodo que recibe el nombre de una mesa, busca esa mesa en la colección y cambia el estado de la mesa a true(ocupado), tambien devulve un Color(rojo).
     * @param nombre
     * 
     */
    public void ocupar(String nombre)
    {
        Iterator<Mesa> it=mesas.iterator();
        while(it.hasNext())
        {
            Mesa m=it.next();
            if(m.getNombre().equalsIgnoreCase(nombre))
            {
                m.setOcupada(true);
            }
        }
        
    }
    
    /**
     * Metodo que recibe el nombre de una mesa, busca esa mesa en la colección y cambia el estado de la mesa a false(desocupada), tambien devulve un Color(verde).
     * @param nombre
     * 
     */
    public void desocupar(String nombre)
    {
        Iterator<Mesa> it=mesas.iterator();
        while(it.hasNext())
        {
            Mesa m=it.next();
            if(m.getNombre().equalsIgnoreCase(nombre))
            {
                m.setOcupada(false);
            }
        }
        
    }
    
    /**
     * Metodo que comprueba si existe un fichero, en caso de existir llama al metodo cargarFichero(), y en caso de que no, creara 4 mesas y las cargara en la colección
     */
    public void arrancarPrograma( )
    {
        File fichero=new File("EstadoMesas.dat");
        if(fichero.exists())
            cargarFichero();
        else
        {
            Mesa m1=new Mesa("mesa1", 2,false);
            Mesa m2=new Mesa("mesa2", 2,false);
            Mesa m3=new Mesa("mesa3", 4,false);
            Mesa m4=new Mesa("mesa4", 6,false);
            añadirmesa(m1);
            añadirmesa(m2);
            añadirmesa(m3);
            añadirmesa(m4);
        }
    }
    
}
