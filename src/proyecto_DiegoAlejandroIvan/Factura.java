/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyecto_DiegoAlejandroIvan;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DAM109
 */
public class Factura 
{
    private int nFactura;
    private int idComida;
    private String nombre;
    private int cantidad;
    private double precio;

    public Factura(int nFactura, int idComida, String nombre, int cantidad, double precio) 
    {
        this.nFactura = nFactura;
        this.idComida = idComida;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getnFactura() {
        return nFactura;
    }

    public void setnFactura(int nFactura) {
        this.nFactura = nFactura;
    }

    public int getIdComida() {
        return idComida;
    }

    public void setIdComida(int idComida) {
        this.idComida = idComida;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }  
}
